<?php

$dom = new DOMDocument();
$xsl = new XSLTProcessor();
$xslFile = $_POST['xsl'];
$dom->load($xslFile);
$xsl->importStylesheet($dom);
$xmlFile = $_POST['xml'];
$dom->load($xmlFile);
echo $xsl->transformToXml($dom);

?>
