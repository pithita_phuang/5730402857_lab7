package phuangsuwan.pithita.lab7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author pithitamacbook
 */
@WebServlet(name = "CallFac", urlPatterns = {"/CallFac"})
public class CallFac extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallFac</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>These are faculties at KKU</h1>");
            out.println("<ul>");
            JSONParser parser = new JSONParser();
            try {
                URL url = new URL("http://www.kku.ac.th/ikku/api/academics/services/getFaculty.php");
                URLConnection urlC = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlC.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String inputLine;
                String jsonStr = "";
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();
                jsonStr = sb.toString();

                JSONObject obj = (JSONObject) parser.parse(jsonStr);
                JSONArray results = (JSONArray) obj.get("results");
                Iterator itr = results.iterator();
                while (itr.hasNext()) {
                    JSONObject innerObj = (JSONObject) itr.next();
                    String title = (String) innerObj.get("name_en");
                    out.println("<li>" + title + "</li>");
                }
            } catch (Exception e) {
                System.err.println("There's an error.");
            }

            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(CallFac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(CallFac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
