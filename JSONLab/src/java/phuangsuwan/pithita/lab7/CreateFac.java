package phuangsuwan.pithita.lab7;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author pithitamacbook
 */
@WebServlet(name = "CreateFac", urlPatterns = {"/CreateFac"})
public class CreateFac extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           JSONParser parser = new JSONParser();
            try {
                URL url = new URL("http://www.kku.ac.th/ikku/api/academics/services/getFaculty.php"); // URL to Parse
                URLConnection urlC = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlC.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String inputLine;
                String jsonStr = "";
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                }
                in.close();
                jsonStr = sb.toString();
                
                JSONObject obj = (JSONObject) parser.parse(jsonStr);
                JSONArray results = (JSONArray) obj.get("results");
                JSONArray faculties = new JSONArray();
                for (int i = 0; i < 2; i++){
                    Random r = new Random();
                    int selInt = r.nextInt((26 - 0) + 1) + 0;
                    JSONObject selObj = (JSONObject)results.get(selInt);
                    JSONObject faculty = new JSONObject();
                    faculty.put("name", selObj.get("name_en"));
                    faculty.put("id", selObj.get("id"));
                    faculties.add(faculty);
                }
                JSONObject myJson = new JSONObject();
                myJson.put("faculties", faculties);
                out.println(myJson);
                
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(CreateFac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(CreateFac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
